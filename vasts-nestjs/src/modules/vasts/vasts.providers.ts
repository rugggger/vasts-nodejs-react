import { Vast } from './vast.entity';

export const vastsProviders = [
    {
        provide: 'VASTS_REPOSITORY',
        useValue: Vast,
    },
];
