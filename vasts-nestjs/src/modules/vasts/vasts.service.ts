import { Injectable, Inject } from '@nestjs/common';
import {Vast} from "./vast.entity";
import xml from 'xml';

@Injectable()
export class VastsService {
    constructor(
        @Inject('VASTS_REPOSITORY') private vastsRepository: typeof Vast) {}

    async findAll(): Promise<Vast[]> {
        return this.vastsRepository.findAll();
    }

    async createVast(values:any): Promise<Vast> {
        return this.vastsRepository.create<Vast>(values);
    }

    async updateVast(id: number, values:any): Promise<Vast> {
        await this.vastsRepository.update(values,
            {where: {id}}
            );
        return this.vastsRepository.findOne({where:{id}});
    }

    async getVastXML(id:number): Promise<any> {

        const vast = await this.vastsRepository.findOne({where:{id}});
        let content = {};
        if (vast) {
            const MediaFile = {
                MediaFile: {_attr: {
                        type:'application/javascript',
                        apiFramework: 'VPAID',
                        height: vast.height,
                        width: vast.width,
                        delivery:"progressive",
                    }, _cdata: `https://cheq.com/vpaid.js?vast=${vast.vast_url}&position=${vast.position}&vastId=${vast.id}`
                }
            };
            content =  {
                Ad: [{
                    InLine: [
                        {AdSystem: '2.0'},
                        {Impression: {}},
                        {
                            Creatives: [{
                                Creative:
                                    [{
                                        Linear:
                                            [{
                                                MediaFiles:
                                                    [MediaFile]
                                            }]
                                    }]
                            }]
                        },
                    ]
                }]
            };
        }




        const  xmlStructure = [ { VAST: [ { _attr: { version: '2.0'} },
                content
            ]
        }];

        return xml(xmlStructure);

    }
}
