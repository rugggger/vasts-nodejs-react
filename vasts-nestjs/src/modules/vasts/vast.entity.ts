import {Table, Column, Model, TableOptions, Sequelize, DataType} from 'sequelize-typescript';

export const POSITION_ENUM = ['top_left', 'top_middle','top_right','middle_left','middle_right',
    'bottom_left','bottom_middle','bottom_right'];

@Table
({modelName:'vasts'})
export class Vast extends Model<Vast> {
    @Column
    ({type: DataType.STRING(600)})
    vast_url: string;

    @Column
    ({
        type: DataType.ENUM(...POSITION_ENUM),
        defaultValue: 'bottom_right'
    })
    position: string;

    @Column
    ({
        type: DataType.INTEGER,
        defaultValue: 100,
        validate: {
            min: 100,
            max: 1000
        }
    })
    width: number;

    @Column
    ({
        type: DataType.INTEGER,
        defaultValue: 100,
        validate: {
            min: 100,
            max: 1000
        }
    })
    height: number;

}


