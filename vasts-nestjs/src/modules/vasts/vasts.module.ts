import { Module } from '@nestjs/common';
import { VastsController } from './vasts.controller';
import {DatabaseModule} from "../database/database.module";
import {vastsProviders} from "./vasts.providers";
import {VastsService} from "./vasts.service";


@Module({
    imports: [DatabaseModule],
    controllers: [VastsController],
    providers: [
        VastsService,
        ...vastsProviders
    ],
})
export class VastsModule {}
