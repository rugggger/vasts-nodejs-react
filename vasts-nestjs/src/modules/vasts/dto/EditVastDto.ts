// @ts-ignore
import {IsEnum, IsInt, IsOptional, IsString, IsUrl, Max, Min} from 'class-validator';
import {POSITION_ENUM} from "../vast.entity";



export class EditVastDto {
    @IsString()
    @IsUrl()
    @IsOptional()
    readonly vastUrl: string;

    @IsOptional()
    @IsEnum(POSITION_ENUM)
    readonly position: string;

    @IsOptional()
    @IsInt()
    @Min(100)
    @Max(1000)
    readonly width: number;

    @IsOptional()
    @IsInt()
    @Min(100)
    @Max(1000)
    readonly height: number;
}
