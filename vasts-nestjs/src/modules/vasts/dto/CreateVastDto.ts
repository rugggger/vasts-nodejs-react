// @ts-ignore
import {IsEnum, IsInt, IsString, IsUrl, Max, Min} from 'class-validator';
import {POSITION_ENUM} from "../vast.entity";

export class CreateVastDto {
    @IsString()
    @IsUrl()
    readonly vastUrl: string;

    @IsEnum(POSITION_ENUM)
    readonly position: string;

    @IsInt()
    @Min(100)
    @Max(1000)
    readonly width: number;

    @IsInt()
    @Min(100)
    @Max(1000)
    readonly height: number;
}
