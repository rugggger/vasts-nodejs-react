import {Body, Controller, Get, Param, Post, Put, Response} from '@nestjs/common';
import {VastsService} from "./vasts.service";
import {Vast} from "./vast.entity";
import {CreateVastDto} from "./dto/CreateVastDto";
import {EditVastDto} from "./dto/EditVastDto";

@Controller('vasts')
export class VastsController {
    constructor(private readonly vastsService: VastsService) {}

    @Get('')
    async getVasts(): Promise<Vast[]> {
       return this.vastsService.findAll();
    }

    @Post('')
    async createVast(@Body() createVastDto: CreateVastDto): Promise<Vast> {

        const values = {
            vast_url: createVastDto.vastUrl,
            position: createVastDto.position,
            width: createVastDto.width,
            height: createVastDto.height
        };

        return this.vastsService.createVast(values);
    }

    @Put(':id')
    async editVast(@Param('id') id: number , @Body() editVastDto: EditVastDto): Promise<Vast> {
        if (editVastDto.vastUrl) {
            editVastDto['vast_url']= editVastDto.vastUrl;
        }
        return this.vastsService.updateVast(id, editVastDto);
    }

    @Get(':id')
    async getVast(@Param('id') id:number, @Response() res) {
        res.set('Content-Type', 'text/xml');
        const xml = await this.vastsService.getVastXML(id);
        res.send(xml);
    }
}
