import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {VastsModule} from "./modules/vasts/vasts.module";

@Module({
  imports: [
      VastsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
