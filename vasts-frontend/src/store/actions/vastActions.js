import {
    FETCH_VASTS,
    SET_VASTS,
    CREATE_VAST,
    EDIT_VAST,
    UPDATE_VAST,
    OPEN_VAST_MODAL,
    CLOSE_VAST_MODAL,
    UPDATE_VAST_SUCCESS
} from './types';
import axios from 'axios';

function fetchVasts() {
    return (dispatch) => {
        axios.get('/vasts')
            .then(
                res => {
                    dispatch({
                        type: SET_VASTS,
                        payload: res.data
                    });
                }
            )
            .catch(err => {
                alert("Couldn't fetch vasts !");
            });


    }
}

function createVast(vast) {
    return (dispatch) => {
        axios.post('/vasts', vast)
            .then(
                res => {
                    if (res.data.success===false) {
                        const errors = res.data.errors.join('\n');
                        alert(`Failed to create vast!\n${errors}`);
                        return;
                    }
                    dispatch({
                        type: CREATE_VAST,
                        payload: res.data
                    });
                }
            )
            .catch(e => {
                alert('Failed to create vast !');
            });

    }
}

function editVast(vast) {

    return {
        type: EDIT_VAST,
        payload: vast
    };
}

function UpdateVast(vast) {
    return (dispatch) => {
        const id = vast.vastId;
        axios.put(`vasts/${id}`, vast)
            .then(
                res => {
                    if (res.data.success===false) {
                        const errors = res.data.errors.join('\n');
                        alert(`Failed to create vast!\n${errors}`);
                        return;
                    }
                    const updatedVast = res.data;
                    dispatch({
                        type: UPDATE_VAST,
                        payload: updatedVast
                    });

                }
            )
            .catch(e => {
                alert('Failed to create vast !');
            });

    }
}

function openVastModal(){
    return {
        type: OPEN_VAST_MODAL
    };
}

function closeVastModal(){
    return {
        type: CLOSE_VAST_MODAL
    }
}
export { fetchVasts, createVast, editVast , UpdateVast, openVastModal, closeVastModal};
