import {
    SET_VASTS, CREATE_VAST, EDIT_VAST, UPDATE_VAST,
    OPEN_VAST_MODAL, CLOSE_VAST_MODAL
} from "../actions/types";

const initialState = {
    vasts: [],
    vastToEdit: null,
    modalOpen: true
};
const vasts = (state = initialState, action) => {
    switch (action.type) {
        case SET_VASTS:
            return {
                ...state,
                vasts: action.payload
            };
        case CREATE_VAST:
            return {
                ...state,
                modalOpen: false,
                vasts: [...state.vasts, action.payload]
            };
        case EDIT_VAST:
            return {
                ...state,
                modalOpen: true,
                vastToEdit: action.payload
            };
        case UPDATE_VAST:
            const vastsWithUpdatedVast =  state.vasts.map((vast) => {
            if (vast.id !== action.payload.id) {
                return vast
            }
            return {
                ...vast,
                ...action.payload
            }});
            return {
                ...state,
                modalOpen: false,
                vastToEdit: null,
                vasts: vastsWithUpdatedVast
            };
        case OPEN_VAST_MODAL:
            return {
                ...state,
                modalOpen: true,
                vastToEdit: null
            };
        case CLOSE_VAST_MODAL:
            return {
                ...state,
                modalOpen: false
            }
        default:
            return state
    }
};

export default vasts;
