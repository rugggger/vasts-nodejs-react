import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './store/store';
import VastsPage from "./VastsPage/VastsPage";

function App() {
  return (
      <Provider store={store}>
          <VastsPage/>
      </Provider>
  );
}

export default App;
