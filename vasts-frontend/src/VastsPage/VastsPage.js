import React from 'react';
import VastsList from "./VastsList";
import {  Container , Button} from '@material-ui/core';
import AddVastModal from "./AddVastModal";
import { useDispatch} from "react-redux";
import {openVastModal} from "../store/actions/vastActions";

const viewXML = (vastId) => {
    const url = `http://localhost:3000/vasts/${vastId}`;
    window.open(url);
};


const VastsPage = ()=>{
    const dispatch = useDispatch();
    return (
        <div>
            <Container style={{marginTop:'2rem'}}>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={()=>{dispatch(openVastModal())}}>Create Vast</Button>
                <AddVastModal/>
                    <VastsList
                        viewXMLCb = {viewXML}
                    />
            </Container>
        </div>
    );
};

export default VastsPage;
