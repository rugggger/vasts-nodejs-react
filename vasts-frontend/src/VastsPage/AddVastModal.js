import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Modal } from '@material-ui/core';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import AddVastForm from "./AddVastForm";
import { useSelector, useDispatch} from "react-redux";
import {closeVastModal} from "../store/actions/vastActions";

const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiTextField-root, & .MuiSelect-root ': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        width:'60rem',
        height:'15rem',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },

}));

export default function AddVastModal() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const modalOpen = useSelector(state => state.vastsReducer.modalOpen);

    return (
        <div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={modalOpen}
                onClose={()=>{dispatch(closeVastModal())}}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={modalOpen}>
                    <div className={classes.paper}>
                        <AddVastForm/>


                    </div>
                </Fade>
            </Modal>
        </div>
    );
}
