import React from "react";
import { Formik } from "formik";
import { TextField , Grid, FormControl, InputLabel, Select, MenuItem, Button } from "@material-ui/core";
import * as Yup from "yup";
import {createVast, editVast, UpdateVast} from "../store/actions/vastActions";
import { useSelector, useDispatch} from "react-redux";
import './Vast.scss';
const AddVastForm = () => {
    const vastsToEdit = useSelector(state => state.vastsReducer.vastToEdit);


    const POSITION_ENUM = ['top_left', 'top_middle','top_right','middle_left','middle_right',
        'bottom_left','bottom_middle','bottom_right'];
    const dispatch = useDispatch();
    const initialValues = {
        vastUrl: "",
        position: 'bottom_left',
        width: 100,
        height: 100
    };

    if (vastsToEdit) {
        initialValues.vastUrl = vastsToEdit.vast_url;
        initialValues.position = vastsToEdit.position;
        initialValues.width = vastsToEdit.width;
        initialValues.height = vastsToEdit.height;
    }

    const menu = POSITION_ENUM.map(o => <MenuItem key={o} value={o}>{o}</MenuItem>);
    return (
            <Formik
                initialValues={initialValues}
                enableReinitialize={true}
                onSubmit={async values => {
                    if (vastsToEdit) {
                        const payload = Object.assign(values, {vastId:vastsToEdit.id});
                        dispatch(UpdateVast(payload));
                    } else {
                        dispatch(createVast(values));
                    }
                }}
                validationSchema={Yup.object().shape({
                    vastUrl: Yup.string()
                        .url()
                        .required("Required"),
                    position: Yup.string(),
                    width: Yup.number()
                        .required("Required")
                        .min(100)
                        .max(1000),
                    height: Yup.number()
                        .required("Required")
                        .min(100)
                        .max(1000)

                })}
            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        dirty,
                        isValid,
                        isSubmitting,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        handleReset
                    } = props;
                    return (
                        <form onSubmit={handleSubmit}>
                            {vastsToEdit? `Updating vast ${vastsToEdit.id}` : 'Adding new vast'}
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth={true}
                                        id="vastUrl"
                                        name="vastUrl"
                                        placeholder="URL"
                                        type="text"
                                        value={values.vastUrl}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={
                                            errors.vastUrl && touched.vastUrl
                                                ? "text-input error"
                                                : "text-input"
                                        }
                                    />
                                    {errors.vastUrl && touched.vastUrl && (
                                        <div className="input-feedback">{errors.vastUrl}</div>
                                    )}
                                </Grid>
                                <Grid item xs={4}>
                                    <FormControl  fullWidth={true}>
                                        <InputLabel id="demo-simple-select-label">Position</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            name="position"
                                            value={values.position}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        >
                                            {menu}
                                        </Select>
                                    </FormControl>
                                </Grid>

                                <Grid item xs={4}>
                                    <TextField
                                        fullWidth={true}
                                        id="width"
                                        label="Width"
                                        type="number"
                                        value={values.width}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={
                                            errors.width && touched.width
                                                ? "text-input error"
                                                : "text-input"
                                        }
                                    />
                                    {errors.width && touched.width && (
                                        <div className="input-feedback">{errors.width}</div>
                                    )}
                                </Grid>

                                <Grid item xs={4}>
                                    <TextField
                                        name="height"
                                        fullWidth={true}
                                        label="Height"
                                        type="number"
                                        value={values.height}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={
                                            errors.height && touched.height
                                                ? "text-input error"
                                                : "text-input"
                                        }
                                    />
                                    {errors.height && touched.height && (
                                        <div className="input-feedback">{errors.height}</div>
                                    )}
                                </Grid>
                                <Grid item xs={12}>
                                    <Button
                                        variant="contained"
                                        type="submit" disabled={isSubmitting || (!isValid)}>
                                        {vastsToEdit? 'Update Vast' : 'Add Vast'}
                                    </Button>
                                </Grid>
                            </Grid>
                        </form>
                    );
                }}
            </Formik>
    );
};

export default AddVastForm;
