import React from 'react';
import { Grid, Button, ListItem, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        listStyle: 'none'
    },
    paper: {
        height: 140,
        width: 100,
    },
    control: {
        padding: theme.spacing(2),
    },
}));

const handleToggle = val => () => {
    console.log('handle ',val);
};
const VastItem = (props) =>{
    const { vast } = props;
    const classes = useStyles();

    return (
       <ListItem key={vast.id} role={undefined} dense button onClick={handleToggle(vast.id)}>
           <ListItemText>{vast.vast_url}</ListItemText>
           <ListItemText>{vast.position}</ListItemText>

       </ListItem>
    );
};

export default VastItem;
