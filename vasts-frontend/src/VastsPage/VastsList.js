import React from 'react';
import { useEffect } from 'react';
import { useSelector, useDispatch} from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import {List, ListItem, ListItemText, Button, ListItemSecondaryAction, ListSubheader, Paper} from '@material-ui/core';
import {fetchVasts, editVast} from "../store/actions/vastActions";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 1000,
        backgroundColor: theme.palette.background.paper,
    },
    item:{
        background:'#f7f7f7',
        marginBottom:'1rem',
       boxShadow: '0rem 0.2rem 0.5rem 0.1rem #0000004d'
    },
    url: {
        width: 300,
        maxWidth:300,
        overflow:'hidden',
        textAlign:'left'
    },
    normalCell: {
        width:150,
        maxWidth:150,
        textAlign:'left'
    },
    actions:{
        width:200,
        maxWidth:200,
        textAlign:'left'
    }
}));


const VastsList = (props)=> {
    const { viewXMLCb  } = props;
    const classes = useStyles();

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchVasts());
    },[]);

    const vasts = useSelector(state => state.vastsReducer.vasts);
    return (
        <List className={classes.root}>
            <ListItem>
                <ListItemText className={classes.url} primary={"URL"} />
                <ListItemText className={classes.normalCell} primary={"Position"} />
                <ListItemText className={classes.normalCell} primary={"Width"} />
                <ListItemText className={classes.normalCell} primary={"Height"} />
                <ListItemText className={classes.actions} primary={"Actions"} />
            </ListItem>
            {vasts.map(vast => {
                return (
                    <ListItem
                        className={classes.item}
                        key={vast.id} role={undefined} dense>
                        <ListItemText className={classes.url} primary={vast.vast_url} />
                        <ListItemText className={classes.normalCell} primary={vast.position} />
                        <ListItemText className={classes.normalCell} primary={vast.width} />
                        <ListItemText className={classes.normalCell} primary={vast.height} />
                        <div className={classes.actions} >
                            <Button
                                variant="outlined"
                                onClick={()=> dispatch(editVast(vast))}>Edit</Button>
                            <Button
                                variant="outlined"
                                onClick={()=>viewXMLCb(vast.id)}>View-XML</Button>
                        </div>
                    </ListItem>
                );
            })}
        </List>
    );
}

export default VastsList;
