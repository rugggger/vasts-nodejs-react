const Sequelize = require('sequelize');
const constants = require('../../constants');

const sequelize = require('../db');

const Vast = sequelize.define('vast',{
    id: {
        type: Sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    vast_url: {
        type: Sequelize.DataTypes.STRING(600)
    },
    position: {
        type:Sequelize.DataTypes.ENUM(constants.POSITION_ENUM),
        defaultValue: 'bottom_right'
    },
    width: {
        type:Sequelize.DataTypes.INTEGER,
        defaultValue: 100,
        validate: {
            min: 100,
            max: 1000
        }
    },
    height: {
        type:Sequelize.DataTypes.INTEGER,
        defaultValue: 100,
        validate: {
            min: 100,
            max: 1000
        }
    },
    createdAt: {
        type: Sequelize.DataTypes.DATE,
        defaultValue: sequelize.literal('NOW()'),
    },
    updatedAt: {
        type: Sequelize.DataTypes.DATE,
        defaultValue: sequelize.literal('NOW()'),
    },

});

module.exports = Vast;
