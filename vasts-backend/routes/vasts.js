const express = require('express');
const router = express.Router();
const vastsController = require('../controllers/vasts');

router.get('/', vastsController.getVasts);
router.get('/:id',  vastsController.getVastXML);
router.post('/', vastsController.addVast);
router.put('/:id', vastsController.editVast);

module.exports = router;
