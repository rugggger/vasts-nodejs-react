class RequestError  {
    constructor(joiValidateObject) {
        this.joiValidateObject = joiValidateObject;
    }

    createErrorObject(){
        const errors = this.joiValidateObject.error.details.map(error=>error.message);
        return {
            success: false,
            request: this.joiValidateObject.value,
            errors

        }
    }
}

class DatabaseError {
    constructor(databaseError) {
        this.databaseError = databaseError;

    }
    createErrorObject(){
        console.log('db ', this.databaseError);
        const errors = this.databaseError.errors.map(error=>error.message);
        return {
            success: false,
            errors
        }
    }

}


module.exports = {
    RequestError,
    DatabaseError
};
