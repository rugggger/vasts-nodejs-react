const Joi = require('@hapi/joi');
const constants = require('../../constants');

const schemaVast = Joi.object({
    vastUrl: Joi.string()
        .uri()
        .required(),
    position: Joi.string()
        .valid(...constants.POSITION_ENUM)
        .optional(),
    width: Joi.number()
        .optional()
        .integer()
        .min(100)
        .max(1000),
    height: Joi.number()
        .optional()
        .integer()
        .min(100)
        .max(1000)

});

const schemaVastPut = Joi.object({
    vastId: Joi.number()
        .required()
        .integer(),
    vastUrl: Joi.string()
        .uri()
        .optional(),
    position: Joi.string()
        .valid(...constants.POSITION_ENUM)
        .optional(),
    width: Joi.number()
        .optional()
        .integer()
        .min(100)
        .max(1000),
    height: Joi.number()
        .optional()
        .integer()
        .min(100)
        .max(1000)

});

module.exports = {
    schemaVast,
    schemaVastPut
};
