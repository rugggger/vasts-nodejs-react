const vastSchema  = require('../validations/schemas/Vast');
const Errors = require('../errors/errors');
const Vast = require('../database/models/Vast');
const xml = require('xml');

exports.addVast = async (req,res) => {
    const body = req.body;
    const validate = vastSchema.schemaVast.validate(body,{ abortEarly: false });
    if (validate.error) {
        res.json(new Errors.RequestError(validate).createErrorObject());
        return;
    }
    try {
        body.vast_url = body.vastUrl;
        let newVast = await Vast.create(body);
        res.json(newVast);
    } catch (e) {
        res.json(new Errors.DatabaseError(e).createErrorObject());
    }

};

exports.editVast = async (req,res) => {
    const id = req.params.id;
    const body = req.body;
    const validate = vastSchema.schemaVastPut.validate(body,{ abortEarly: false });
    if (validate.error) {
        res.json(new Errors.RequestError(validate).createErrorObject());
        return;
    }
    try {
        body.vast_url = body.vastUrl;
        await Vast.update(body, {
            where: { id}
        });
        let updatedVast = await Vast.findOne({ where: {id}});
        res.json(updatedVast);
    } catch (e) {
        res.json(new Errors.DatabaseError(e).createErrorObject());
    }
};

exports.getVasts = async (req, res) => {
    try {
        const vasts = await Vast.findAll();
        res.json(vasts);
    }
    catch (e) {
        res.json(new Errors.DatabaseError(e).createErrorObject());
    }
};


exports.getVastXML = async (req, res) => {
    try {
        const id = req.params.id;
        const vast = await Vast.findOne({where:{id}});
        let content = {};
        if (vast) {
            const MediaFile = {
                MediaFile: {_attr: {
                        type:'application/javascript',
                        apiFramework: 'VPAID',
                        height: vast.height,
                        width: vast.width,
                        delivery:"progressive",
                    }, _cdata: `https://cheq.com/vpaid.js?vast=${vast.vast_url}&position=${vast.position}&vastId=${vast.id}`
                }
            };
            content =  {
                Ad: [{
                    InLine: [
                        {AdSystem: '2.0'},
                        {Impression: {}},
                        {
                            Creatives: [{
                                Creative:
                                    [{
                                        Linear:
                                            [{
                                                MediaFiles:
                                                    [MediaFile]
                                            }]
                                    }]
                            }]
                        },
                    ]
                }]
            };
        }




        const  xmlStructure = [ { VAST: [ { _attr: { version: '2.0'} },
                content
            ]
        }];


        res.set('Content-Type', 'text/xml');
        res.send(xml(xmlStructure));
    }
    catch (e) {
        res.json(new Errors.DatabaseError(e).createErrorObject());
    }
};
